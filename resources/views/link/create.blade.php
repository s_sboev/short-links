@extends('link.master')
@section('title','ShortUrl')
@section('content')
    <div class="article-form">
        <form action="{{route('store')}}" method="POST">
            @csrf
            @method('POST')
            <ul class="form__list">
                <li class="form__item">
                    <label class='form__label' for="url">URL</label>
                    <input class='form__input' id='url' name='url' type="text"
                    @if ($link ?? '')
                       value="{{$link->url}}"
                    @endif
                    >
                </li>
                <li class="form__item">
                    <label class='form__label' for="token">Short Link</label>
                    <input class='form__input' id='token' name='token' type="text"
                    @if ($link ?? '')
                        value="{{request()->getSchemeAndHttpHost().'/'.$link->token}}"
                    @endif
                    disabled>
                </li>
            </ul>
            <button type="submit">Generate</button>
        </form>
    </div>
@endsection
